#include "DisplayBuffer.h"
#include "WProgram.h"

// --------------------------------------
// ctor. Ensure lines are blank filled and no garbage characters.
DisplayBuffer::DisplayBuffer()
  {
  memset(mLine1, ' ', sizeof(mLine1));
  memset(mLine2, ' ', sizeof(mLine2));
  }

// --------------------------------------
// clear the screen
void DisplayBuffer::clear()
  {
  Serial.print(0x01, BYTE);
  }

// --------------------------------------
// sound the beeper
void DisplayBuffer::beep()
  {
  Serial.print(0x07, BYTE);
  }

// --------------------------------------
// turn on the LED
void DisplayBuffer::ledOn()
  {
  Serial.print(0x11, BYTE);
  }

// --------------------------------------
// turn off the LED
void DisplayBuffer::ledOff()
  {
  Serial.print(0x12, BYTE);
  }

// --------------------------------------
// send each full line buffer to the LCD
// always sends 32 characters so the buffers here and the LCD buffers
// don't ever get out of sync
void DisplayBuffer::send()
  {
  // send line1 buffer
  for (int col = 0; col < 16; col++)
    {
    Serial.print(mLine1[col], BYTE);
    }

  // send line2 buffer
  for (int col = 0; col < 16; col++)
    {
    Serial.print(mLine2[col], BYTE);
    }
  }

// --------------------------------------
// set the line1 buffer to the given buffer
void DisplayBuffer::setLine1(const char* buf)
  {
  setLine(mLine1, buf);
  }

// --------------------------------------
// set the line2 buffer to the given buffer
void DisplayBuffer::setLine2(const char* buf)
  {
  setLine(mLine2, buf);
  }

// --------------------------------------
// set the line buffer to the given buffer
// ensure the rest of the line is blank filled
void DisplayBuffer::setLine(char* line, const char* buf)
  {
  for (int col = 0; col < 16; col++)
    {
    if (*buf)
      {
      line[col] = *buf++;
      }
    else
      {
      line[col] = ' ';
      }
    }
  }
